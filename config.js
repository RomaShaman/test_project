module.exports = {
    server: {
        host: '0.0.0.0',
        port: 2204
    },
    swaggerHost: 'localhost'
}