const Hapi = require('@hapi/hapi');
const Inert = require('@hapi/inert');
const Vision = require('@hapi/vision');
const HapiSwagger = require('hapi-swagger');
const Package = require('../package');
const filepaths = require('filepaths');
const hapiBoomDecorators = require('hapi-boom-decorators');
const config = require('../config');

const swaggerOptions = {
    info: {
        title: Package.name + ' API Documentation',
        description: Package.description
    },
    jsonPath: '/documentation.json',
    documentationPath: '/documentation',
    schemes: ['https', 'http'],
    host: config.swaggerHost,
    debug: true
};

async function createServer(logLVL=config.logLVL) {

    const server = await new Hapi.Server(config.server);

    await server.register([
        hapiBoomDecorators,
        Inert,
        Vision,
        {
            plugin: HapiSwagger,
            options: swaggerOptions
        },
    ]);


    let routes = filepaths.getSync(__dirname + '/routes/');

    // костыль для модификации путей (необходим для запуска под виндой) 
    for (let route of routes) {
        server.route(require(route.replace(__dirname + '\\', '').replace(__dirname + '/', '')));
    }
    // конец костыля

    try {
        await server.start();
        console.log(`Server running at: ${server.info.uri}`);
    } catch(err) { 
        console.log(JSON.stringify(err));
    }

    return server;
}

module.exports = createServer; 