async function response() {
  return {
    result: 'ok',
    message: 'HomePage'
  };
}

module.exports = {
  method: 'GET',
  path: '/', 
  options: { 
    handler: response
  }
};