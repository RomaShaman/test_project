const Joi = require('joi');
const Handler = require('../../handlers/people');


module.exports = {
  method: 'GET',
  path: '/people/{id}',
  options: {
    handler: Handler.response,
    tags: ['api'],
    validate: {
      params: Joi.object({
        id: Joi.number().integer().required().example(1)
      })
    },
  }
};