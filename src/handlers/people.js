const Db = require('../../db');

module.exports = {
    response: function response(request) {
        let response = Db.people[request.params.id-1];
        if (response === undefined) {
            return {
                result: 'ok',
                error: 404,
                response: 'This people not found',
            };
        }
        else {
            return {
                result: 'ok',
                response: response
            };
        }
    }
}